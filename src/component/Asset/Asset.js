import React, { Component } from "react";
import { Link } from 'react-router-dom';

import "./Asset.scss";

export default class Asset extends Component {
    render() {
        return (
            <div className="c-asset col-md-3 col-sm-6 col-xs-12">
                <div className="card">
                    <img className="card-img-top" src={this.props.item.image} alt="Card cap" />
                    <div className="card-body">
                        <h5 className="card-title">{this.props.item.title}</h5>
                        <p className="card-text">{this.props.item.shortDescription()}</p>
                        <Link to={`/asset/${this.props.item.id}`} className="btn btn-primary">Learn More</Link>
                    </div>
                </div>
            </div>
        );
    }
}