import React, { Component } from "react";

import "./Filter.scss";

export default class Filter extends Component {
    constructor(props) {
        super(props);
        this.updateKeyword = this.updateKeyword.bind(this);
        this.updateType = this.updateType.bind(this);
        this.makeSearch = this.makeSearch.bind(this);

        this.state = {
            keyword: '',
            type: 'image'
        }
    }
    makeSearch() {
        this.props.onSearch({
            keyword: this.state.keyword,
            type: this.state.type
        });
    }
    updateKeyword(e) {
        this.setState({
            keyword: e.target.value,
            type: this.state.type
        });
    }
    updateType(e) {
        this.setState({
            keyword: this.state.keyword,
            type: e.target.value
        });
    }
    render() {
        return (
            <div className="c-filter text-left">
                <div className="row">
                    <div className="col-6">
                        <div className="form-group">
                            <label htmlFor="keyword">Keyword:</label>
                            <input className="form-control" value={this.state.keyword} onChange={this.updateKeyword} placeholder="Type keyword to search for..." />
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="form-group">
                            <label htmlFor="keyword">Type:</label>
                            <select className="form-control" value={this.state.type} onChange={this.updateType}>
                                <option value="image">Image</option>
                                <option value="audio">Audio</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="form-group">
                            <button className="btn btn-success btn-block" onClick={this.makeSearch}>Search</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}