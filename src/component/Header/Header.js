import React from 'react';
import { Link } from "react-router-dom";

import "./Header.scss";

const Header = () => {
    return (
        <header className="c-header py-3">
            <div className="align-items-center">
            <Link to="/">
                <img src="https://www.elveflow.com/wp-content/uploads/2015/10/NASA-logo.png" className="c-header--logo" alt="NASA Logo" />
            </Link>
            NASA Assets
            </div>
        </header>
    )
}

export default Header;