import React, { Component } from "react";
import Asset from "../Asset/Asset";

import "./ItemList.scss";
export default class ItemList extends Component {
    render() {
        return (
            <div className="c-item-list row">
                {this.props.items.length === 0 
                    ? <div className="c-item-list--empty">NASA can't give you everything, please, specify keyword...</div>
                    : this.props.items.map((item) => {
                        return <Asset item={item} key={item.id} />
                    })
                }
            </div>
        );
    }
}