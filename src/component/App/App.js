import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import ListPage from '../../page/ListPage/ListPage';
import SingleItemPage from '../../page/SingleItemPage/SingleItemPage';

class App extends Component {
  render() {
      return (
        <Switch>
            <Route exact path='/' component={ListPage}/>
            <Route path='/asset/:id' component={SingleItemPage}/>
        </Switch>
      );
  }
}

export default App;
