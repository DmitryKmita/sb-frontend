import axios from "axios";
import querystring from "querystring";
import AssetHydrator from "../hydrator/AssetHydrator";

const apiUrl = "https://images-api.nasa.gov/search";

class ApiService {
    getAsset(id) {
        return new Promise((success, error) => {
            let params = querystring.stringify({
                'nasa_id': id
            });
            axios
            .get(apiUrl + "?" + params)
            .then(response => {
                let list = [];
                response.data.collection.items.forEach((item) => {
                    list.push(AssetHydrator.hydrateAsset(item));
                });
                success(list[0]);
            })
            .catch(err => {
                error(err);
            });
        });
    }
    searchAssets(term, type) {
        return new Promise((success, error) => {
            let params = querystring.stringify({
                'q': term,
                'media_type': type
            });

            axios
            .get(apiUrl + "?" + params)
            .then(response => {
                let list = [];
                response.data.collection.items.forEach((item) => {
                    list.push(AssetHydrator.hydrateAsset(item));
                });
                success(list);
            })
            .catch(err => {
                error(err);
            });
        });
    }
}

export default ApiService;