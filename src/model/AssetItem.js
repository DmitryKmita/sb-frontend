
export default class AssetItem {
    constructor() {
        this.id = "";
        this.title = "";
        this.description = "";
        this.image = "";
        this.keywords = [];
    }

    shortDescription(maxLength = 80) {
        if (this.description.length < maxLength) {
            return this.description;
        } else {
            return this.description.slice(0, maxLength) + "...";
        }
    }
}