import AssetItem from "../model/AssetItem";

const AssetHydrator = {
    hydrateAsset: function(item) {
        let assetItem = new AssetItem();
        assetItem.id = item.data[0].nasa_id;
        if (item.data[0].description !== undefined) {
            assetItem.description = item.data[0].description;
        }
        assetItem.title = item.data[0].title;
        assetItem.keywords = item.data[0].keywords;
        if (item.links !== undefined && item.links[0].render === 'image') {
            assetItem.image = item.links[0].href;
        } else {
            switch (item.data[0].media_type) {
                case 'audio':
                    assetItem.image = "/images/sound-bg.png";
                    break;
                default:
                    assetItem.image = "/images/unknown-bg.png";
                    break;
            }
        }
        return assetItem;
    }
};

export default AssetHydrator;