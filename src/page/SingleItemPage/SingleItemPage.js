import React, { Component } from "react";
import ApiService from '../../service/ApiService';
import Header from '../../component/Header/Header';
import AssetItem from "../../model/AssetItem";

import "./SingleItemPage.scss";

export default class SingleItemPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          asset: new AssetItem()
        };
        this.getAsset(props.match.params.id);
    }

    getAsset(id) {
        let apiService = new ApiService();
        apiService
          .getAsset(id)
          .then((asset) => {
            this.setState({
              asset: asset
            });
          });
    }

    render() {
        return (
            <div className="SingleItemPage c-single">
                <div className="container">
                    <Header />
                    <div className="row">
                        <div className="col-6">
                            <img src={this.state.asset.image} className="c-single--image" alt={this.state.asset.title} />
                        </div>
                        <div className="col-6">
                            <h1 className="c-single--title">{this.state.asset.title}</h1>
                            <p className="c-single--description">{this.state.asset.description}</p>
                            <div className="c-single-keywords">
                                 { this.state.asset.keywords.map((keyword) => {
                                     return (
                                         <div key={keyword} className="c-single-keywords--keyword badge badge-secondary">{keyword}</div>
                                     );
                                 }) }
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}