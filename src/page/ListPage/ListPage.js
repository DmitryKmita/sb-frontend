import React, { Component } from "react";
import ApiService from '../../service/ApiService';
import Header from '../../component/Header/Header';
import Filter from '../../component/Filter/Filter';
import ItemList from '../../component/ItemList/ItemList';

export default class ListPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          assets: []
        };
        this.searchItems = this.searchItems.bind(this);
        this.searchItems({ keyword: '', 'type': 'image' });
      }
      searchItems(e) {
        let apiService = new ApiService();
        apiService
          .searchAssets(e.keyword, e.type)
          .then((assets) => {
            this.setState({
              assets: assets
            });
          });
    }
    render() {
        return (
            <div className="ListPage">
                <div className="container">
                    <Header />
                    <Filter onSearch={this.searchItems} />
                    <ItemList items={this.state.assets} />
                </div>
            </div>
        );
    }
}