# React Frontend Application for NASA Assets

To run this application (need nodeJS + npm installed):

* Install dependencies:
```
npm install
```
* Run the application:
```
npm start
```

To run this application on production (need nodeJS + npm installed):

* Install dependencies:
```
npm install
```
* Build the application:
```
yarn build
```
* Serve the application (then proxy via nginx for example):
```
serve -s build
```

# Notes:

* Because NASA has very weird nasa_id (object identifiers) with spaces - it sometimes breaks the url and gets 400 from NASA on Single Page Application
* Application is PWA ready
* Used BEM for CSS naming convention
* I have ejected webpack config to add .scss compiler
* Reason I used axios instead of native https - I love promises.
* I have restricted myself to 2.5h of work, but have defined a list of future changes I would consider

# Possible next steps:

* Clickable tags on Single page to search for assets by keyword.
* Gallery on Single Page. Some of the assets return a list of images, would be nice to show the gallery straight away
* Video preview. Some assets have videos, would be nice to add a preview.
* Go Back should save the search query (could use hash History API on search)
* Better search form. I could prebuild some form fields NASA Centre, what could have been used in filters + added input fields with photographers etc.
* Service worker to cache results, to make application work offline.
* Create infinite pagination on List Page, so that the more you scroll - more items it prefetches.
* Similar items on Single Page. Because we have asset keywords - we can make a search on other items regarding this keyword and provide customers a carousel of similar assets for them to view.